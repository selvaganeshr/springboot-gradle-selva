package com.cts.controller;

import com.cts.dao.RecordActivityDao;
import com.cts.entity.AppActivityLogger;
import com.cts.exceptions.BadRequestException;
import com.cts.exceptions.ForbiddenException;
import com.cts.exceptions.NotFoundException;
import com.cts.exceptions.UnauthorizedException;
import com.cts.services.NASAService;
import com.cts.templates.MarsRoverPhotos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Date;

@Controller
@CrossOrigin(origins = "*")
@RequestMapping("/getMarsPhotos")
public class NasaAssignmentController {

	@Autowired
	private NASAService nasaService;

	@RequestMapping("/sol/{solId}")
	public ResponseEntity<MarsRoverPhotos> getMarsRoverPhotos(@PathVariable Integer solId) throws UnauthorizedException, NotFoundException, BadRequestException, ForbiddenException {
		System.out.println("getMarsRoverPhotos"+solId);
		System.out.println("getMarsRoverPhotos"+nasaService);
		MarsRoverPhotos marsRoverPhotos = nasaService.getMarsPhotosSol(solId);
		if(marsRoverPhotos.getPhotos().isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<MarsRoverPhotos>(marsRoverPhotos, HttpStatus.OK);
	}

	@RequestMapping("/sol/{solId}/camera/{cameraId}")
	public ResponseEntity<MarsRoverPhotos> getMarsRoverPhotosWithCameraParam(@PathVariable Integer solId, @PathVariable String cameraId) throws UnauthorizedException, NotFoundException, BadRequestException, ForbiddenException {
		MarsRoverPhotos marsRoverPhotos = nasaService.getMarsPhotosSolCamera(solId, cameraId);
		if(marsRoverPhotos.getPhotos().isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<MarsRoverPhotos>(marsRoverPhotos, HttpStatus.OK);
	}

}
