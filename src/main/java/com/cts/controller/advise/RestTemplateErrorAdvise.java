package com.cts.controller.advise;

import com.cts.exceptions.BadRequestException;
import com.cts.exceptions.ForbiddenException;
import com.cts.exceptions.NotFoundException;
import com.cts.exceptions.UnauthorizedException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.UnknownHostException;

@ControllerAdvice
public class RestTemplateErrorAdvise {

    @ExceptionHandler(UnknownHostException.class)
    public void handleError(HttpServletResponse response, UnknownHostException e) throws IOException {
        response.sendError(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Dependent API service is not available.");
    }

    @ExceptionHandler(UnauthorizedException.class)
    public void handleError(HttpServletResponse response, UnauthorizedException e) throws IOException {
        response.sendError(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED.getReasonPhrase());
    }

    @ExceptionHandler(ForbiddenException.class)
    public void handleError(HttpServletResponse response, ForbiddenException e) throws IOException {
                response.sendError(HttpStatus.FORBIDDEN.value(), e.getLocalizedMessage());
    }

    @ExceptionHandler(BadRequestException.class)
    public void handleError(HttpServletResponse response, BadRequestException e) throws IOException {
        response.sendError(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.getReasonPhrase());
    }

    @ExceptionHandler(NotFoundException.class)
    public void handleError(HttpServletResponse response, NotFoundException e) throws IOException {
        response.sendError(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND.getReasonPhrase());
    }
}
