package com.cts.exceptions;

public class NotFoundException extends RuntimeException {

    public NotFoundException(String msg) {
        super(msg);
    }

    public NotFoundException(String errorId, String msgFormat, Object... msgArgs){
        this("'; Error Id: '" + errorId + "'\n" + String.format(msgFormat, msgArgs));
    }

    public NotFoundException() {
    }
}

