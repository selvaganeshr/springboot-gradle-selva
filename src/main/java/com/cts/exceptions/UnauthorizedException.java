package com.cts.exceptions;

public class UnauthorizedException extends RuntimeException {
    public UnauthorizedException(String msg) {
        super(msg);
    }

    public UnauthorizedException(String errorId, String msgFormat, Object... msgArgs){
        this("'; Error Id: '" + errorId + "'\n" + String.format(msgFormat, msgArgs));
    }

    public UnauthorizedException() {
    }
}
