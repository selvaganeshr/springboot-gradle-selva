package com.cts.services;

import com.cts.properties.UrlProperties;
import com.cts.templates.MarsRoverPhotos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import com.cts.constants.NASAApi;

@Service
public class NASAServiceImpl implements NASAService{

    public enum Camera {
        FHAZ,
        RHAZ,
        MAST,
        CHEMCAM,
        MAHLI
    }

    @Autowired
    private UrlProperties urlProperties;

    public MarsRoverPhotos getMarsPhotosSol(int sol) {
        String baseUrl = urlProperties.getNasaApi() + "mars-photos/api/v1/rovers/curiosity/photos?sol="+sol+"&api_key="+NASAApi.APIKEY;
        RestTemplate restTemplate = new RestTemplate();
        MarsRoverPhotos result = restTemplate.getForObject(baseUrl, MarsRoverPhotos.class);
        return result;
    }

    public MarsRoverPhotos getMarsPhotosSolCamera(int sol, String camera) {
        MarsRoverPhotos result = new MarsRoverPhotos();
        if(camera.equals(Camera.FHAZ.toString()) || camera.equals(Camera.RHAZ.toString()) || camera.equals(Camera.MAST.toString())
                || camera.equals(Camera.CHEMCAM.toString()) || camera.equals(Camera.MAHLI.toString()) || camera.equals(Camera.FHAZ.toString().toLowerCase()) ||
                camera.equals(Camera.RHAZ.toString().toLowerCase()) || camera.equals(Camera.MAST.toString().toLowerCase()) || camera.equals(Camera.CHEMCAM.toString().toLowerCase()) ||
                        camera.equals(Camera.MAHLI.toString().toLowerCase())) {
            String baseUrl = urlProperties.getNasaApi() + "mars-photos/api/v1/rovers/curiosity/photos?sol=" + sol + "&camera=" + camera + "&api_key=" + NASAApi.APIKEY;
            RestTemplate restTemplate = new RestTemplate();
            result = restTemplate.getForObject(baseUrl, MarsRoverPhotos.class);
            return result;
        }
        return result;
    }
}
