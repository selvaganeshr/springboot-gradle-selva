package com.cts.services;

import com.cts.templates.MarsRoverPhotos;

import java.util.List;

public interface NASAService {
    public MarsRoverPhotos getMarsPhotosSol(int sol);
    public MarsRoverPhotos getMarsPhotosSolCamera(int sol, String camera);
}
