package com.cts.aop;

import com.cts.dao.RecordActivityDao;
import com.cts.entity.AppActivityLogger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import java.util.Date;

@Aspect
@Component
public class LoggingAspect
{
    @Autowired
    private RecordActivityDao recordActivityDao;
    long i=0;

    @Around("execution(* com.cts.controller..*(..)))")
    public Object profileAllMethods(ProceedingJoinPoint proceedingJoinPoint) throws Throwable
    {
        MethodSignature methodSignature = (MethodSignature) proceedingJoinPoint.getSignature();

        String className = methodSignature.getDeclaringType().getSimpleName();
        String methodName = methodSignature.getName();

        final StopWatch stopWatch = new StopWatch();

        stopWatch.start();
        Object result = proceedingJoinPoint.proceed();
        stopWatch.stop();

        AppActivityLogger appActivityLogger = new AppActivityLogger(i++, methodName, stopWatch.getTotalTimeMillis(), new Date());
        System.out.println(appActivityLogger.toString());
        recordActivityDao.save(appActivityLogger);

        return result;
    }
}