package com.cts.templates;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import jdk.nashorn.internal.objects.annotations.Getter;
import lombok.Data;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@ToString
@Data
public class MarsRoverPhotos {
    @JsonProperty("photos")
    private List<Photos> photos = new ArrayList<>();

    /*public MarsRoverPhotos(List<Photos> photos){
        this.photos = photos;
    }*/

    @JsonProperty("photos")
    public List<Photos> getPhotos() {
        if (photos == null) {
            photos = new ArrayList<>();
        }
        return photos;
    }

    @JsonProperty("photos")
    public void setPhotos(List<Photos> photos) {
        if (photos == null) {
            this.photos = new ArrayList<>();
            return;
        }
        this.photos = photos;
    }
}
