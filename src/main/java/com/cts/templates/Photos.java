package com.cts.templates;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.ToString;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@ToString
public class Photos {
    @JsonProperty("id")
    private Integer id;
    @JsonProperty("sol")
    private Integer sol;
    @JsonProperty("camera")
    private RoverCamera roverCamera;
    @JsonProperty("img_src")
    private String imgSrc;
    @JsonProperty("earth_date")
    private String earthDate;

    /*public Photos(Integer id, Integer sol, RoverCamera roverCamera, String imgSrc, String earthDate) {
        this.id = id;
        this.sol = sol;
        this.roverCamera = roverCamera;
        this.imgSrc = imgSrc;
        this.earthDate = earthDate;
    }*/

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("sol")
    public Integer getSol() {
        return sol;
    }

    @JsonProperty("sol")
    public void setSol(Integer sol) {
        this.sol = sol;
    }

    @JsonProperty("camera")
    public RoverCamera getRoverCamera() {
        return roverCamera;
    }

    @JsonProperty("camera")
    public void setRoverCamera(RoverCamera roverCamera) {
        this.roverCamera = roverCamera;
    }

    @JsonProperty("img_src")
    public String getImgSrc() {
        return imgSrc;
    }

    @JsonProperty("img_src")
    public void setImgSrc(String imgSrc) {
        this.imgSrc = imgSrc;
    }

    @JsonProperty("earth_Date")
    public String  getEarthDate() {
        return earthDate;
    }

    @JsonProperty("earth_Date")
    public void setEarthDate(String earthDate) {
        this.earthDate = earthDate;
    }


}
