package com.cts.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "app_activity_log")
public class AppActivityLogger {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Column(name="methodName", length=50)
    private String methodName;
    private long responseTime;
    private Date createdDate;

    public AppActivityLogger() {
        this.methodName = "";
        this.responseTime = 0;
        this.createdDate = new Date();
    }

    public AppActivityLogger(long id, String methodName, long responseTime, Date createdDate) {
        this.id=id;
        this.methodName = methodName;
        this.responseTime = responseTime;
        this.createdDate = createdDate;
    }

    public long getId() {
        return id;
    }

    public long getResponseTime() {
        return responseTime;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public String getMethodName() {
        return methodName;
    }

    @Override
    public String toString() {
        return "AppLogger{" + "id=" + id + ", methodName=" + methodName + ", responseTime=" + responseTime + ", createdDate=" + createdDate + '}';
    }
}