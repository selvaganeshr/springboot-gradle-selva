package com.cts.dao;

import com.cts.entity.AppActivityLogger;

import java.sql.Connection;

public interface RecordActivityDao {
    public Connection connect();

    public void save(AppActivityLogger tAppActivityLog);

}