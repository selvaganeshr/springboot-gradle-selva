package com.cts.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;

import com.cts.dao.RecordActivityDao;
import com.cts.entity.AppActivityLogger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class RecordActivityDaoImpl implements RecordActivityDao {
    final static Logger logger = LoggerFactory.getLogger(RecordActivityDaoImpl.class);

    public Connection connect() {
        String url = "jdbc:sqlite:mydb.db";//+dbFile;
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url);
            logger.info("Connected to Sqlite database");
        } catch (SQLException e) {
            logger.info(e.getMessage());
        }
        return conn;
    }

    public  void save(AppActivityLogger tAppActivityLog) {
        String sql = "INSERT INTO app_activity_log (id, methodName, responseTime, createdDate) "
                + "values (?,?,?,?)";

        try (Connection conn = this.connect();
            PreparedStatement pstmt = conn.prepareStatement(sql)) {

            //Calendar calendar = Calendar.getInstance();
            java.util.Date today = new java.util.Date();

            pstmt.setLong(1, tAppActivityLog.getId());
            pstmt.setString(2, tAppActivityLog.getMethodName());
            pstmt.setLong(3, tAppActivityLog.getResponseTime());
            pstmt.setDate(4, new java.sql.Date(today.getTime()));
            pstmt.executeUpdate();
            logger.info("API Method Activity recorded");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }


}