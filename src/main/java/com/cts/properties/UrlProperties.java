package com.cts.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:application.properties")
@ConfigurationProperties(prefix = "url", ignoreUnknownFields = true)
public class UrlProperties {
    private String nasaApi;

    public String getNasaApi() {
        return nasaApi;
    }

    public void setNasaApi(String nasaApi) {
        this.nasaApi = nasaApi;
    }
}
